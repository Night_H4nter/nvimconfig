""LSP settings and mappings



"no idea it was in readme
" set omnifunc=lsp#complete
" set tagfunc=lsp#tagfunc


"disable vim-lsp diagnostics
let g:lsp_diagnostics_enabled = 0


"set keymaps
nmap <space>id <plug>(lsp-definition)
nmap <space>is <plug>(lsp-document-symbol-search)
nmap <space>iS <plug>(lsp-workspace-symbol-search)
nmap <space>ir <plug>(lsp-references)
nmap <space>ii <plug>(lsp-implementation)
nmap <space>it <plug>(lsp-type-definition)
nmap <space>iH <plug>(lsp-hover)
nmap <space>iR <plug>(lsp-rename)




" nnoremap <plug>(lsp-call-hierarchy-incoming)
" nnoremap <plug>(lsp-call-hierarchy-outgoing)
" nnoremap <plug>(lsp-code-action)
" nnoremap <plug>(lsp-code-lens)
" nnoremap <plug>(lsp-declaration)
" nnoremap <plug>(lsp-peek-declaration)
" nnoremap <plug>(lsp-definition)
" nnoremap <plug>(lsp-peek-definition)
" nnoremap <plug>(lsp-document-symbol)
" nnoremap <plug>(lsp-document-symbol-search)
" nnoremap <plug>(lsp-document-diagnostics)
" nnoremap <plug>(lsp-hover)
" nnoremap <plug>(lsp-next-diagnostic)
" nnoremap <plug>(lsp-next-diagnostic-nowrap)
" nnoremap <plug>(lsp-next-error)
" nnoremap <plug>(lsp-next-error-nowrap)
" nnoremap <plug>(lsp-next-reference)
" nnoremap <plug>(lsp-next-warning)
" nnoremap <plug>(lsp-next-warning-nowrap)
" nnoremap <plug>(lsp-preview-close)
" nnoremap <plug>(lsp-preview-focus)
" nnoremap <plug>(lsp-previous-diagnostic)
" nnoremap <plug>(lsp-previous-diagnostic-nowrap)
" nnoremap <plug>(lsp-previous-error)
" nnoremap <plug>(lsp-previous-error-nowrap)
" nnoremap <plug>(lsp-previous-reference)
" nnoremap <plug>(lsp-previous-warning)
" nnoremap <plug>(lsp-previous-warning-nowrap)
" nnoremap <plug>(lsp-references)
" nnoremap <plug>(lsp-rename)
" nnoremap <plug>(lsp-workspace-symbol)
" nnoremap <plug>(lsp-workspace-symbol-search)
" nnoremap <plug>(lsp-document-format)
" vnoremap <plug>(lsp-document-format)
" nnoremap <plug>(lsp-document-range-format)
" xnoremap <plug>(lsp-document-range-format)
" nnoremap <plug>(lsp-implementation)
" nnoremap <plug>(lsp-peek-implementation)
" nnoremap <plug>(lsp-type-definition)
" nnoremap <plug>(lsp-peek-type-definition)
" nnoremap <plug>(lsp-type-hierarchy)
" nnoremap <plug>(lsp-status)
" nnoremap <plug>(lsp-signature-help)

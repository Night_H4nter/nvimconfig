--Main configuraion file for Neovim by Daniel "Night_H4nter".
--For license, see LICENSE file


--NEVER change the section loading order unless you really know what you're
--doing, otherwise something may break, misbehave, misrender, work slower, etc.

--Conf ALWAYS goes first, util (if any) goes second, key goes last


--correct loading order is:
--   1.  [ section plugins ]
--   2.  [ section ui ]
--   3.  [ section lsp ]
--   3.  [ section functions ]
--   5.  [ section submodules ]
--   6.  [ section keymappings ]




--wrapper function for sourcing viml files
local function vimlsource(...)
    vim.cmd('source ~/.config/nvim/viml/' .. ...)
end




-- [ section plugins ]
vim.cmd('source ~/.config/nvim/plugins.vim')


-- [ section primary ui ]
require('ui_prim.treesitter')
require('ui_prim.vanilla')
require('ui_prim.whichkey')
vimlsource('ui_prim/statusline.vim')
vimlsource('util/tmux-crutch.vim')


-- [ section secondary ui ]
vimlsource('ui_sec/undotree.vim')
require('ui_sec.nerdtree')
vimlsource('util/ui.vim')
require('ui_sec.telescope')
require('ui_sec.maximizer')
vimlsource('ui_sec/tagbar.vim')


-- [ section core ]
require('general.vanilla')
require('general.leaders')
vimlsource('util/shadashare.vim')


-- [ section intellisense ]
vimlsource('util/alternate.vim')
vimlsource('intellisense/completion.vim')
vimlsource('intellisense/lsp.vim')
require('lspconfig.sumneko_lua')
require('lspconfig.clangd')
require('lspconfig.haskell-language-server')


-- [ section navigation ]
require('navigation.buffer')
require('navigation.cross-buffer')


-- [ section tertiary ui ]
require('ui_ter.hexokinase')

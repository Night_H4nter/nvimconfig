----In-buffer navigation bindings



--basic navigation
vim.api.nvim_set_keymap( '', 'j', 'h', { noremap = true } )
vim.api.nvim_set_keymap( '', 'k', 'j', { noremap = true } )
vim.api.nvim_set_keymap( '', 'l', 'k', { noremap = true } )
vim.api.nvim_set_keymap( '', ';', 'l', { noremap = true } )


--move cursor to the bottom/top of current screen
vim.api.nvim_set_keymap( '', 'K', 'L', { noremap = true } )
vim.api.nvim_set_keymap( '', 'L', 'H', { noremap = true } )


--disable arrows in normal mode
vim.api.nvim_set_keymap( 'n', '<Up>', '<NOP>', { noremap = true } )
vim.api.nvim_set_keymap( 'n', '<Down>', '<NOP>', { noremap = true } )
vim.api.nvim_set_keymap( 'n', '<Left>', '<NOP>', { noremap = true } )
vim.api.nvim_set_keymap( 'n', '<Right>', '<NOP>', { noremap = true } )


--disable "normal" window navigation
vim.api.nvim_set_keymap( 'n', '<c-w>h', '<NOP>', { noremap = true } )
vim.api.nvim_set_keymap( 'n', '<c-w>j', '<NOP>', { noremap = true } )
vim.api.nvim_set_keymap( 'n', '<c-w>k', '<NOP>', { noremap = true } )
vim.api.nvim_set_keymap( 'n', '<c-w>l', '<NOP>', { noremap = true } )
vim.api.nvim_set_keymap( 'n', '<c-w>w', '<NOP>', { noremap = true } )


--make spamming v/V in visual mode
--expand/shrink selection region
vim.api.nvim_set_keymap( 'v', 'v', '<plug>(expand_region_expand)', {} )
vim.api.nvim_set_keymap( 'v', 'V', '<plug>(expand_region_shrink)', {} )


--smart tabs
-- local function t(str)
--     return vim.api.nvim_replace_termcodes(str, true, true, true)
-- end

-- function _G.smart_tab()
--     return vim.fn.pumvisible() == 1 and t'<C-n>' or t'<Tab>'
-- end

-- function _G.smart_shifttab()
--     return vim.fn.pumvisible() == 1 and t'<C-p>' or t'<Tab>'
-- end

-- vim.api.nvim_set_keymap('i', '<Tab>', 'v:lua.smart_tab()', {expr = true, noremap = true})

-- vim.api.nvim_set_keymap('i', '<S-Tab>', 'v:lua.smart_shifttab()', {expr = true, noremap = true})


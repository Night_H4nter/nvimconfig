----NerdTree UI settings



--change expandable/collapsible indicators
vim.g.NERDTreeDirArrowExpandable = '+'
vim.g.NERDTreeDirArrowCollapsible = '-'


--fix crashes if calling vimplug funcions while nerdtree window is focused
vim.g.plug_window = 'noautocmd vertical topleft new'


--use nerdfonts for git integration
vim.g.NERDTreeGitStatusUseNerdFonts = 1


--show clean indicator
vim.g.NERDTreeGitStatusShowClean = 1



--toggle nerdtree
vim.api.nvim_set_keymap( 'n', '<Bslash>uf', ':NERDTreeToggle<cr>',
    { noremap = true, silent = true })



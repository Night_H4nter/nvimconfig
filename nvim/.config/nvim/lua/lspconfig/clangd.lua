local coq = require "coq"


require'lspconfig'.clangd.setup
{
    coq.lsp_ensure_capabilities
    {
        -- on_attach = require('intellisense.key.keys'),
        cmd = { 'clangd', '--background-index', '--cross-file-rename',
            '--header-insertion=iwyu', '-j=4'},
        update_in_insert = true,
    }
}
